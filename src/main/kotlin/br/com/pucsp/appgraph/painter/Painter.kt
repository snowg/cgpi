package br.com.pucsp.appgraph.painter

import br.com.pucsp.appgraph.model.*
import br.com.pucsp.appgraph.model.graph.*
import br.com.pucsp.appgraph.model.mat.*
import java.awt.Graphics

class Painter {

    private var mode: PainterMode = PainterMode.IDLE
    val drawer = Drawer()
    private lateinit var drawnableObject: DrawnableObject

    fun paint(point: Point, graphics: Graphics){
        when(this.mode){
            PainterMode.IDLE -> {}
            else -> {
                drawnableObject.pushPoint(point)
                if(drawnableObject.isDrawnable()){
                    drawer.draw(drawnableObject.getDrawnablePoints(),graphics)
                    changeMode(this.mode)
                }
            }
        }
    }

    fun paint(graphics: Graphics){
        when(this.mode){
            PainterMode.IDLE -> {}
            else -> {
                if(drawnableObject.isDrawnable()){
                    drawer.draw(drawnableObject.getDrawnablePoints(),graphics)
                    changeMode(this.mode)
                }
            }
        }
    }

    fun changeMode(newMode: PainterMode) {
        this.mode = newMode
        when (mode) {
            PainterMode.POINT -> {
                drawnableObject = GraphicPoint()
            }
            PainterMode.STRAIGHT -> {
                drawnableObject = GraphicStraight()
            }
            PainterMode.CIRCLE -> {
                drawnableObject = GraphicCircle()
            }
            PainterMode.RECT -> {
                drawnableObject = GraphicRect()
            }
        }
    }

    fun changeMode(model: MatModel) {
        when (model) {
            is Circle -> {
                this.mode = PainterMode.CIRCLE
                this.drawnableObject = GraphicCircle(model)
            }
            is Straight -> {
                this.mode = PainterMode.STRAIGHT
                this.drawnableObject = GraphicStraight(model)
            }
            is Point -> {
                this.mode = PainterMode.POINT
                this.drawnableObject = GraphicPoint(model)
            }
            is Rect -> {
                this.mode = PainterMode.POINT
                this.drawnableObject = GraphicRect(model)
            }
        }
    }

}