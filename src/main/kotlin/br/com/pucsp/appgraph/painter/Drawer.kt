package br.com.pucsp.appgraph.painter

import br.com.pucsp.appgraph.model.mat.Point
import java.awt.Color
import java.awt.Graphics

class Drawer {

    var color: Color = Color.decode("#000000")
    var size: Int = 2
    private var sizeDivider: Int = 2


    fun draw(points: Array<Point>, graphics: Graphics){
        points.forEach {
            draw(it,graphics)
        }
    }

    fun draw(point: Point, graphics: Graphics){
        graphics.color = this.color
        graphics.fillOval(
            point.x - size / sizeDivider,
            point.y - size /sizeDivider,
            size,
            size
        )
        //Thread.sleep(5)
    }

}