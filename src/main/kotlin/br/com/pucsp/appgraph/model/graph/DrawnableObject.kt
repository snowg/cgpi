package br.com.pucsp.appgraph.model.graph

import br.com.pucsp.appgraph.model.mat.Point

interface DrawnableObject {

    fun pushPoint(point: Point)

    fun isDrawnable(): Boolean

    fun getDrawnablePoints(): Array<Point>

}