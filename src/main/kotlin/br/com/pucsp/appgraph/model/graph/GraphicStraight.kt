package br.com.pucsp.appgraph.model.graph

import br.com.pucsp.appgraph.model.mat.Point
import br.com.pucsp.appgraph.model.mat.Straight
import kotlin.math.abs

class GraphicStraight(): DrawnableObject {

    private val points = mutableListOf<Point>()
    private lateinit var straight: Straight

    constructor(p1: Point, p2: Point) : this() {
        this.straight = Straight(p1, p2)
    }

    constructor(straight: Straight): this() {
        this.straight = straight
    }

    override fun pushPoint(point: Point) {
        this.points.add(point)
        if (points.size == 2) {
            this.straight = Straight(points[0], points[1])
        }
    }

    override fun isDrawnable(): Boolean {
        return ::straight.isInitialized
    }

    override fun getDrawnablePoints(): Array<Point> {
        return genPoints().toTypedArray()
    }

    private fun genPoints(): MutableList<Point>{

        val list = mutableListOf<Point>()
        val p1 = straight.p1
        val p2 = straight.p2

        if(p1.x == p2.x){
            if(p1.y < p2.y){
                for(yAc in p1.y..p2.y){
                    list.add(Point(p1.x, yAc))
                }
            }else {
                for(yAc in p2.y..p1.y){
                    list.add(Point(p1.x, yAc))
                }
            }
        }else if(p1.y == p2.y){
            if(p1.x < p2.x){
                for(xAc in p1.x..p2.x){
                    list.add(Point(xAc, p1.y))
                }
            }else {
                for(xAc in p2.x..p1.x){
                    list.add(Point(xAc, p1.y))
                }
            }

        }else {
            val ang = abs(straight.m)
            if ( ang > 1 ){
                if(p1.y < p2.y){
                    for(xi in p1.y..p2.y){
                        val y = straight.getX(xi)
                        list.add(Point(y, xi))
                    }
                }else {
                    for(xi in p2.y..p1.y){
                        val y = straight.getX(xi)
                        list.add(Point(y, xi))
                    }
                }
            }else {
                if(p1.x < p2.x){
                    for(xi in p1.x..p2.x){
                        val y = straight.getY(xi)
                        list.add(Point(xi, y))
                    }
                }else {
                    for(xi in p2.x..p1.x){
                        val y = straight.getY(xi)
                        list.add(Point(xi, y))
                    }
                }
            }


        }
        return list
    }
}