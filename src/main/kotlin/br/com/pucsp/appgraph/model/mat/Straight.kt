package br.com.pucsp.appgraph.model.mat

class Straight(val p1: Point, val p2: Point): MatModel {

    val m = m()
    val b = b()

    fun getX(y: Int): Int{
        return ((y - b) / m).toInt()
    }

    fun getY(x: Int): Int {
        return ((m * x) + b).toInt()
    }


    private fun b(): Double {
        return p1.y - m * p1.x
    }

    fun m(): Double {
        val y = (p2.y - p1.y).toDouble()
        val x = (p2.x - p1.x).toDouble()
        return y / x
    }

}