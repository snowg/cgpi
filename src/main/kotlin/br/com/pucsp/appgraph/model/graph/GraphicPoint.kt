package br.com.pucsp.appgraph.model.graph

import br.com.pucsp.appgraph.model.mat.Point

class GraphicPoint(): DrawnableObject {

    private lateinit var point: Point

    constructor(point: Point) : this() {
        this.point = point
    }

    constructor(x: Int, y: Int) : this() {
        this.point = Point(x, y)
    }

    fun setPoint(x: Int,y: Int){
        this.point = Point(x, y)
    }

    fun setPoint(point: Point){
        this.point = point
    }

    override fun pushPoint(point: Point) {
        this.point = point
    }

    override fun isDrawnable(): Boolean {
        return ::point.isInitialized
    }

    override fun getDrawnablePoints(): Array<Point> {
        return arrayOf(this.point)
    }
}