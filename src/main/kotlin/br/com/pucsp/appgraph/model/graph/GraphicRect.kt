package br.com.pucsp.appgraph.model.graph

import br.com.pucsp.appgraph.model.mat.Point
import br.com.pucsp.appgraph.model.mat.Rect
import br.com.pucsp.appgraph.model.mat.Straight

class GraphicRect: DrawnableObject {
    val points = mutableListOf<Point>()
    private lateinit var rect: Rect

    constructor(rect: Rect){
        this.rect = rect
    }

    constructor()

    override fun pushPoint(point: Point) {
        this.points.add(point)
        if(this.points.size == 2){
            rect = Rect(points[0],points[1])
        }
    }

    override fun isDrawnable(): Boolean {
        return ::rect.isInitialized
    }

    override fun getDrawnablePoints(): Array<Point> {
        val p2 = Point(rect.p1.x,rect.p2.y)
        val p4 = Point(rect.p2.x,rect.p1.y)

        val straights = listOf( GraphicStraight(Straight(rect.p1,p2)),GraphicStraight(Straight(p2,rect.p2)),GraphicStraight(Straight(rect.p2,p4)),GraphicStraight(Straight(p4,rect.p1)))

        val points = mutableListOf<Point>()

        straights.forEach { straight ->
            run {
                points.addAll(straight.getDrawnablePoints())
            }
        }

        return points.toTypedArray()
    }
}