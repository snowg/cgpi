package br.com.pucsp.appgraph.model.mat

import kotlin.math.pow
import kotlin.math.sqrt

class Point(var x: Int, var y: Int): MatModel{

    fun distance(another: Point): Double{
        val xcalc = (another.x - this.x).toDouble()
        val ycalc = (another.y - this.y).toDouble()

        val xel = xcalc.pow(2.0)
        val yel = ycalc.pow(2.0)
        return sqrt(xel+yel)
    }
}