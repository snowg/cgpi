package br.com.pucsp.appgraph.model.mat

class Circle(val center: Point,val radius: Double) : MatModel {

    fun degreeToCoord(degree: Double): Point{
        val rad = Math.toRadians(degree)

        val x = this.radius * Math.cos(rad) + center.x
        val y = this.radius * Math.sin(rad) + center.y

        return Point(x.toInt(),y.toInt())
    }

}