package br.com.pucsp.appgraph.model.graph

import br.com.pucsp.appgraph.model.mat.Circle
import br.com.pucsp.appgraph.model.mat.Point
import br.com.pucsp.appgraph.model.mat.Straight

class GraphicCircle(): DrawnableObject {

    private val points = mutableListOf<Point>()
    private lateinit var circle: Circle

    constructor(p1: Point, p2: Point) : this() {
        //this.circle = Straight(p1, p2)
    }

    constructor(circle: Circle): this() {
        this.circle = circle
    }

    override fun pushPoint(point: Point) {
        this.points.add(point)
        if (points.size == 2) {
            this.circle = Circle(points[0], points[0].distance(points[1]))
        }
    }

    override fun isDrawnable(): Boolean {
        return ::circle.isInitialized
    }

    override fun getDrawnablePoints(): Array<Point> {
        val points = mutableListOf<Point>()

        var angle = 0.0
        while (angle <= 360){
            points.add(circle.degreeToCoord(angle))
            angle += 0.1
        }

        return points.toTypedArray()
    }
}