package br.com.pucsp.appgraph.model

enum class PainterMode {
    IDLE,
    POINT,
    STRAIGHT,
    CIRCLE,
    RECT
}