package br.com.pucsp.appgraph.gui

import br.com.pucsp.appgraph.model.PainterMode
import br.com.pucsp.appgraph.custom.CircleAndStraights
import br.com.pucsp.appgraph.custom.Circles
import java.awt.Color
import javax.swing.*
import javax.swing.JColorChooser.createDialog
import javax.swing.JDialog
import javax.swing.JOptionPane





class InitialGUI: JFrame() {

    private val customTitle = "Paint"
    private val size = Pair(800,600)
    private val closeOp =  EXIT_ON_CLOSE
    private val resize = false

    private var chart = ChartGUI()

    init {
        initialConfig()
        createGUIComponents()
    }

    private fun initialConfig(){
        title = customTitle
        setSize(size.first,size.second)
        defaultCloseOperation = closeOp
        isVisible = true
        isResizable = resize
        setLocationRelativeTo(null)
    }

    private fun createGUIComponents() {

        val topToolBar = JToolBar()
        val midToolBar = JToolBar()

        createTopBarButtons().forEach {
            topToolBar.add(it)
        }

        creatMidToolBarButtons().forEach{
            midToolBar.add(it)
        }

        createLayout(topToolBar,midToolBar,chart)

    }

    private fun createTopBarButtons(): Array<JButton> {

        val btnPoint = JButton("Ponto")

        btnPoint.addActionListener { this.chart.painter.changeMode(PainterMode.POINT) }


        val btnStraight = JButton("Reta")

        btnStraight.addActionListener { this.chart.painter.changeMode(PainterMode.STRAIGHT) }


        val btnCircle = JButton("Circulo")

        btnCircle.addActionListener { this.chart.painter.changeMode(PainterMode.CIRCLE) }

        val btnCircles = JButton("Circulos")
        btnCircles.addActionListener {
            val pane = JOptionPane.showInputDialog(null, "Numero de iterações: ", "Circulos", JOptionPane.QUESTION_MESSAGE)
            Circles(chart).draw(pane.toInt() - 1 )
        }

        val btnRect = JButton("Retangulo")
        btnRect.addActionListener {
            this.chart.painter.changeMode(PainterMode.RECT)
        }

        val btnCirclesAndStraights = JButton("Circulos e retas")
        btnCirclesAndStraights.addActionListener { CircleAndStraights(chart).draw() }

        return arrayOf(
            btnPoint,btnStraight,btnCircle,btnCircles,btnCirclesAndStraights,btnRect
        )
    }

    private fun creatMidToolBarButtons(): Array<JButton> {
        val btnLimparTela = JButton("Limpar Tela")

        btnLimparTela.addActionListener { this.chart.clean() }

        val btnAlterarCor = JButton("Alterar Cor")

        btnAlterarCor.addActionListener {
            val pane = JOptionPane.showInputDialog(null, "Digite o valor da cor em hexadecimal: ", "Alterar Cor", JOptionPane.QUESTION_MESSAGE)
            this.chart.painter.drawer.color = Color.decode("#$pane")
        }

        val btnAlterarTamanho = JButton("Alterar Tamanho")

        btnAlterarTamanho.addActionListener {
            val pane = JOptionPane.showInputDialog(null, "Digite o tamanho: ", "Alterar tamanho", JOptionPane.QUESTION_MESSAGE)
            this.chart.painter.drawer.size = pane.toInt()
        }

        return arrayOf( btnLimparTela,btnAlterarTamanho,btnAlterarCor )
    }

    private fun createLayout(vararg  arg: JComponent){
        val gl = GroupLayout(contentPane)
        contentPane.layout = gl

        gl.autoCreateContainerGaps = true

        gl.setHorizontalGroup(gl.createParallelGroup()
            .addComponent(arg[0])
            .addComponent(arg[1])
            .addComponent(arg[2])
        )

        gl.setVerticalGroup(gl.createSequentialGroup()
            .addComponent(arg[0])
            .addComponent(arg[1])
            .addComponent(arg[2])
        )

        //pack()
    }



}