package br.com.pucsp.appgraph.gui

import br.com.pucsp.appgraph.model.PainterMode
import br.com.pucsp.appgraph.model.mat.Point
import br.com.pucsp.appgraph.painter.Painter
import java.awt.Color
import java.awt.Graphics
import java.awt.event.MouseEvent
import java.awt.event.MouseListener
import javax.swing.BorderFactory
import javax.swing.JPanel

class ChartGUI: JPanel(), MouseListener {

    val painter = Painter()
    private lateinit var lastPoint: Point

    init {
        createUI()
    }

    private fun createUI(){
        this.addMouseListener(this)
        this.border = BorderFactory.createLineBorder(Color.black)
        background = Color.WHITE
    }

    override fun mouseReleased(e: MouseEvent) {}

    override fun mouseEntered(e: MouseEvent) {}

    override fun mouseClicked(e: MouseEvent) {}

    override fun mouseExited(e: MouseEvent) {}

    override fun mousePressed(e: MouseEvent) {
        lastPoint = Point(e.x, e.y)
        paintChart()
    }

    fun clean() {
        this.graphics.clearRect(0,0,this.width,this.height)
        this.createUI()
//        this.painter.changeMode(PainterMode.POINT)
//        this.painter.drawer.color = Color.WHITE
//        for(x in 0 until this.width){
//            for(y in 0 until this.height){
//                this.painter.paint(Point(x,y),this.graphics)
//            }
//        }
//
//        this.painter.drawer.color = Color.BLACK
//        this.border = BorderFactory.createLineBorder(Color.black)
    }

    private fun paintChart(){
        painter.paint(lastPoint,graphics)
    }
}