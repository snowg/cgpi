package br.com.pucsp.appgraph.custom

import br.com.pucsp.appgraph.gui.ChartGUI
import br.com.pucsp.appgraph.model.mat.Circle
import br.com.pucsp.appgraph.model.mat.Point

class Circles(val chartGUI: ChartGUI) {
    fun draw(rounds: Int) {
        val circles = mutableListOf<Circle>()

        val initial = genInitialCircle()

        var last = mutableListOf(initial)

        for (i in 0 until rounds){
            val temp = mutableListOf<Circle>()
            last.forEach{
                temp.addAll(genCorrelatedCircle(it))
            }
            circles.addAll(last)
            last.clear()
            last.addAll(temp)
        }

        circles.addAll(last)


        circles.forEach { circle ->
            chartGUI.painter.changeMode(circle)
            chartGUI.painter.paint(chartGUI.graphics)
        }
    }

    private fun genInitialCircle(): Circle {

        val initialX = chartGUI.width / 2
        val initialY = chartGUI.height / 2

        val p1 = Point(initialX,initialY)

        val secX = ((chartGUI.width / 3) + (chartGUI.width * 0.02)).toInt()
        val secY = ((chartGUI.height / 3) + (chartGUI.width * 0.02)).toInt()

        val p2 = Point(secX,secY)

        return Circle(p1,p2.distance(p1))
    }

    private fun genCorrelatedCircle(baseCircle: Circle): List<Circle> {
        val angleList = listOf(0.0,90.0,180.0,270.0)
        val generatedCircles = mutableListOf<Circle>()

        angleList.forEach { angle ->
            val generated = baseCircle.degreeToCoord(angle)
            val radius = baseCircle.center.distance(generated) / 2
            val circle = Circle(generated,radius)
            generatedCircles.add(circle)
        }

        return generatedCircles
    }
}