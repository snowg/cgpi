package br.com.pucsp.appgraph.custom

import br.com.pucsp.appgraph.gui.ChartGUI
import br.com.pucsp.appgraph.model.mat.Circle
import br.com.pucsp.appgraph.model.mat.Point
import br.com.pucsp.appgraph.model.mat.Straight
import java.awt.Color

class CircleAndStraights(val chartGUI: ChartGUI){

    fun draw() {

        val initialCircle = genInitialCircle()

        val circleList = mutableListOf(initialCircle)

        val outList = genCorrelatedCircle(initialCircle)

        circleList.addAll(outList)

        circleList.forEach { circle ->
            chartGUI.painter.changeMode(circle)
            chartGUI.painter.paint(chartGUI.graphics)
        }

        val points = getBaseCircleLinePoints(baseCircle = initialCircle)

        chartGUI.painter.drawer.color = Color.RED

        val straightList = mutableListOf<Straight>()

        straightList.addAll(getBaseCircleStraights(points))

        straightList.addAll(genOutCircleStraights(outList))

        straightList.forEach {
            chartGUI.painter.changeMode(it)
            chartGUI.painter.paint(chartGUI.graphics)
        }

        chartGUI.painter.drawer.color = Color.BLUE
        chartGUI.painter.drawer.size = chartGUI.painter.drawer.size * 2

        straightList.forEach {
            chartGUI.painter.changeMode(it.p2)
            chartGUI.painter.paint(chartGUI.graphics)
        }

        chartGUI.painter.drawer.size = chartGUI.painter.drawer.size / 2
    }

    private fun genCorrelatedCircle(baseCircle: Circle): MutableList<Circle> {
        val angleList = listOf(0.0,60.0,120.0,180.0,240.0,300.0)
        val generatedCircles = mutableListOf<Circle>()

        angleList.forEach { angle ->
            val generated = baseCircle.degreeToCoord(angle)
            generatedCircles.add(Circle(generated,baseCircle.center.distance(generated)))
        }

        return generatedCircles
    }

    private fun genOutCircleStraights(circleList: List<Circle>): List<Straight> {
        val list = mutableListOf<Straight>()

        val angles = listOf(300.0,60.0, 120.0, 240.0)

        for(i in 0 until circleList.size) {
            val circle = circleList[i]
            for(f in 0 until angles.size){

                val sum  = (i * 60)
                val angle = angles[f] + sum
                if(f == 0){
                    val next = angles[f+1] + sum
                    list.add(Straight(circle.degreeToCoord(angle),circle.degreeToCoord(next)))

                    val circleIndex = (i + 4) % 6
                    val insideAngle = angles[f] + circleIndex * 60
                    val inside = circleList[circleIndex].degreeToCoord(insideAngle)

                    list.add(Straight(circle.degreeToCoord(angle),inside))
                }

                list.add(Straight(circle.center,circle.degreeToCoord(angle)))
            }
        }

        return list
    }

    private fun getBaseCircleLinePoints(baseCircle: Circle): List<Point>{

        val angles = listOf(0.0,60.0,120.0,180.0,240.0,300.0)

        val points = mutableListOf<Point>()

        angles.forEach{
            points.add(baseCircle.degreeToCoord(it))
        }
        return points
    }

    private fun getBaseCircleStraights(list: List<Point>): List<Straight> {

        val straightList = mutableListOf<Straight>()


        for(i in 0 until list.size){
            straightList.add(Straight(list[i],list[(i+1) % 6]))

            straightList.add(Straight(list[i],list[(i+3) % 6 ]))

            straightList.add(Straight(list[i],list[(i+5) % 6]))
        }



        return straightList
    }

    private fun genInitialCircle(): Circle {

        val initialX = chartGUI.width / 2
        val initialY = chartGUI.height / 2

        val p1 = Point(initialX,initialY)

        val secX = ((chartGUI.width / 3) + (chartGUI.width * 0.02)).toInt()
        val secY = ((chartGUI.height / 3) + (chartGUI.width * 0.02)).toInt()

        val p2 = Point(secX,secY)

        return Circle(p1,p2.distance(p1))
    }
}