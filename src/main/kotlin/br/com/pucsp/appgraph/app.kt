package br.com.pucsp.appgraph

import br.com.pucsp.appgraph.gui.InitialGUI
import javax.swing.SwingUtilities


fun main(){
    SwingUtilities.invokeLater { createAndShowGUI() }
}

private fun createAndShowGUI(){
    InitialGUI()
}